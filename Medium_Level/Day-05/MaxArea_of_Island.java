/*
Code 2: Max Area of Island

Company: Amazon
Platform: Leetcode - 695
Fraz’s and Love Bubbar’s DSA sheet.
Description:
You are given an m x n binary matrix grid. An island is a group of 1's (representing land)
connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are
surrounded by water.
The area of an island is the number of cells with a value 1 in the island.
Return the maximum area of an island in grid. If there is no island, return 0.

Example 1:
Input: grid =
[[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,
1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,
0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]
Output: 6
Explanation: The answer is not 11, because the island must be connected 4-directionally.
Example 2:
Input: grid = [[0,0,0,0,0,0,0,0]]
Output: 0

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 50
grid[i][j] is either 0 or 1.
*/

class Solution {

    static int maxAreaOfIsland(int[][] g) {
        
        int r = g.length, c = g[0].length, m = 0;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                m = Math.max(m, task(g, i, j, r, c));
            }
        }
        return m;
    }

    static int task(int g[][], int i, int j, int r, int c) {

        if (i < 0 || j < 0 || i == r || j == c || g[i][j] == 0 || g[i][j] == 2)
            return 0;
        g[i][j] = 2;
        int m = 1 + task(g, i, j - 1, r, c);
        m += task(g, i, j + 1, r, c);
        m += task(g, i - 1, j, r, c);
        m += task(g, i + 1, j, r, c);
        return m;
    }

    public static void main(String[] args) {

        int arr[][] = { { 0, 0, 0, 0, 0, 0, 0, 0 } };

        System.out.println(Solution.maxAreaOfIsland(arr));

    }
}