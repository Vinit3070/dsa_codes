/*

Code 1:Product of Array Except Self
Company: Amazon, Facebook, Microsoft, Goldman Sachs,Qualcomm
Platform: leetcode-238
Fraz’s SDE sheet.
Description:
Given an integer array nums, return an array answer such that answer[i] is
equal to the product of all the elements of nums except nums[i].
The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit
integer.
You must write an algorithm that runs in O(n) time and without using the
division operation.
Example 1:
Input: nums = [1,2,3,4]
Output: [24,12,8,6]
Example 2:
Input: nums = [-1,1,0,-3,3]
Output: [0,0,9,0,0]

Constraints:
2 <= nums.length <= 105
-30 <= nums[i] <= 30
The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

*/

class Solution {
    static int[] productExceptSelf(int[] nums) {

        int left[] = new int[nums.length];
        int right[] = new int[nums.length];
        int ans[] = new int[nums.length];

        int n = nums.length;

        int prod = 1;

        for (int i = 0; i < n; i++) {

            left[i] = prod;
            prod = prod * nums[i];

        }

        prod = 1;

        for (int i = n - 1; i >= 0; i--) {

            right[i] = prod;
            prod = prod * nums[i];

        }
        for (int i = 0; i < n; i++) {

            ans[i] = left[i] * right[i];
            
        }

        return ans;
    }

    public static void main(String[] args) {

        int arr[] = { 1, 2, 3, 4 };

        int res[] = Solution.productExceptSelf(arr);

        for (int x : res) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
