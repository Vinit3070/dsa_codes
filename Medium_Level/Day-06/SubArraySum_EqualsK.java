/*
Code 1: Subarray Sum Equals K (Subarray with given sum-GFG)
Company: Amazon, Facebook, Google, Visa
Platform: Leetcode- 560, GFG, Coding Ninjas
Striver’s DSA sheet
Description
Given an array of integers nums and an integer k, return the total number of subarrays
whose sum equals to k.
A subarray is a contiguous non-empty sequence of elements within an array.
Example 1:
Input: nums = [1,1,1], k = 2
Output: 2
Example 2:
Input: nums = [1,2,3], k = 3
Output: 2

Constraints:
1 <= nums.length <= 2 * 104
-1000 <= nums[i] <= 1000
-107 <= k <= 107
*/

class Solution {

    static int findSubArraySum(int Arr[], int N, int k) {

        int count = 0;

        for (int i = 0; i < N; i++) {

            int sum = 0;

            for (int j = i; j < N; j++) {

                sum=sum+Arr[j];

                if(sum==k){
                    count++;
                }
            }
        }

        return count;
    }

    public static void main(String[] args) {

        int arr[] = {1,2,3};

        System.out.println(Solution.findSubArraySum(arr, arr.length, 3));
    }
}
