/*
Code2: Element with left side smaller and right side greater
Company: Zoho, Amazon, OYO Rooms, Intuit
Platform: GFG
Description :
Given an unsorted array of size N. Find the first element in the array such that all
of its left elements are smaller and all right elements are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.

Example 1:
Input:
N = 4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on left of 5 are smaller than 5
and on right of it are greater than 5.

Example 2:
Input:
N = 3
A[] = {11, 9, 12}
Output:
-1

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:
3 <= N <= 106
1 <= A[i] <= 106
*/
import java.util.*;
class Solution{

    static int findElement(int arr[], int n){

        int left[]=new int[n];
        int right[]=new int[n];

        left[0]=arr[0];
        right[n-1]=arr[n-1];

        int l=arr[0];
        for(int i=1;i<n;i++){
            if(arr[i]>l)
                l=arr[i];
            
            left[i]=l;
        }

        int small=arr[n-1];
        for(int i=n-2;i>=0;i--){

            if(arr[i]<small)
                small=arr[i];

            right[i]=small;
        }

        for(int i=1;i<n-1;i++){
            if(right[i]==left[i])
                return right[i];
        }
        return -1;
    }

    public static void main(String[] args) {
        
        int arr[]={11, 9, 12};
        int N=arr.length;
        
        System.out.println(Solution.findElement(arr, N));
        
    }
}