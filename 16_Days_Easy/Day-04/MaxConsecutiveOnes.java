/*

Code 1: Max Consecutive Ones

Company: Google, Facebook, Amazon, Microsoft, Apple, Uber, Airbnb, Adobe, Goldman Sachs,
Bloomberg
Platform: Leetcode - 485, Coding Ninja
Striver’s SDE Sheet
Description:
Given a binary array nums, return the maximum number of consecutive 1's in the array.
Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The
maximum number of consecutive 1s is 3.
Example 2:
Input: nums = [1,0,1,1,0,1]
Output: 2

Constraints:
1 <= nums.length <= 105
nums[i] is either 0 or 1.

*/

class Solution{

    static int findMaxConsecutiveOnes(int[] nums,int n) {
        
        int maxCnt=0;
        int count=0;
        
        for(int i=0; i<n;i++){
            
            if(nums[i]==1){
                count++;
            }
            else{

                if(count>maxCnt){
                    maxCnt=count;
                }
                count=0;
            }
        }

        if(count>maxCnt){
            maxCnt=count;
        }
        return maxCnt;
    }
    public static void main(String[] args) {
        
        int arr[]={1,0,1,1,0,1};

        System.err.println(Solution.findMaxConsecutiveOnes(arr,arr.length));

    }
}