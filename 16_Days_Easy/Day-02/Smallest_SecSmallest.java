/*
Code3 : Find the smallest and second smallest element in
an array
Company: Amazon, Goldman Sachs
Platform: GFG
Description:
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.

Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1<=N<=105
1<=A[i]<=105

*/

class Solution{

    static long[] smAndSecSmallest(long[] arr,int n){
       
        long max=Long.MAX_VALUE;
        long small=max;
        long secSmall=max;

        for(int i=0;i<n;i++){
            if(arr[i]<small){
                small=arr[i];
            }
        }

        for(int i=0;i<n;i++){

            if(arr[i]<secSmall && arr[i]>small){

                secSmall=arr[i];
            }
        }
        if(secSmall==max)
            return new long[]{-1};
        else
            return new long[]{small,secSmall};

    }
    public static void main(String[] args) {
        
        long arr[]={1,2,1,3,6,7,0};
        long res[]=Solution.smAndSecSmallest(arr,arr.length);
       
        for(int i=0;i<res.length;i++){
            System.out.print(res[i]+" ");
        }
    }
}