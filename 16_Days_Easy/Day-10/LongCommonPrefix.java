/*
Code 2: Longest Common prefix in a Array
Company: VMWare, Microsoft, Google
Platform: leetcode-14

Description:
Write a function to find the longest common prefix string amongst an array of
strings.
If there is no common prefix, return an empty string "".
Example 1:
Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:
Input: strs = ["dog", "racecar", "car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Constraints:
1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lowercase English letters.
*/

import java.util.*;
class Solution{

    static String longestCommonPrefix(String[] strs) {
        
        Arrays.sort(strs);

        String str="";

        for(int j=0;j<strs[0].length();j++){

            for(int i=0; i<strs.length; i++){

                if(strs[0].charAt(j)!=strs[i].charAt(j)){
                    return str;   
                }

            }

            str=str+strs[0].charAt(j);
        }
        
        return str;
    }

    public static void main(String[] args) {

        
        String carr[]={"dog", "racecar", "car"};

        System.out.println(Solution.longestCommonPrefix(carr));

    }
}
