
/*
 Code3 : Chocolate Distribution problem
Company : Flipkart
Platform : GFG
Love Bubbars’s SDE sheet
Description :
Given an array A[ ] of positive integers of size N, where each value represents
the number of chocolates in a packet. Each packet can have a variable number of
chocolates. There are M students, the task is to distribute chocolate packets among M
students such that :
1. Each student gets exactly one packet.
2. The difference between maximum number of chocolates given to a student and
minimum number of chocolates given to a student is minimum.
Example 1:
Input:
N = 8, M = 5
A = {3, 4, 1, 9, 56, 7, 9, 12}
Output: 6
Explanation: The minimum difference between maximum chocolates and
minimum chocolates is 9 - 3 = 6 by choosing the following M packets :{3, 4, 9, 7,
9}.
Example 2:
Input:
N = 7, M = 3
A = {7, 3, 2, 4, 9, 12, 56}
Output: 2
Explanation: The minimum difference between maximum chocolates and
minimum chocolates is 4 - 2 = 2 by choosing the following M packets :{3, 2, 4}.

Expected Time Complexity: O(N*Log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ T ≤ 100
1 ≤ N ≤ 105
1 ≤ Ai ≤ 109
1 ≤ M ≤ N
 */
import java.util.*;

class Solution {

  static long findMinDiff(ArrayList<Integer> a, int n, int m) {

    Collections.sort(a);
    int r = m - 1;
    int l = 0;
    long res = Integer.MAX_VALUE;
    if (n == m) {
      return a.get(n - 1) - a.get(0);
    }
    while (r < n) {
      res = Math.min(res, a.get(r) - a.get(l));
      r++;
      l++;

    }
    return res;
  }

  public static void main(String[] args) {

    int arr[] = { 7, 3, 2, 4, 9, 12, 56 };

    ArrayList<Integer> a = new ArrayList<>();

    for (int i : arr) {
      a.add(i);
    }
    System.out.println(findMinDiff(a, arr.length, 3));

  }
}
