/*
Code 1: Intersection of Two Arrays
Company: Accolite, Amazon, Microsoft, PayPal, Rockstand
Platform: Leetcode - 349
Striver’s SDE sheet
Given two integer arrays nums1 and nums2, return an array of their intersection.
Each element in the result must be unique and you may return the result in any
order.
Example 1:
Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]
Example 2:
Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.
Constraints:
1 <= nums1.length, nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 1000
*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class Solution {

    static int[] intersection(int[] nums1, int[] nums2) {

        Set<Integer> s = new HashSet<Integer>();

        for (int i = 0; i < nums1.length; i++) {
            s.add(nums1[i]);
        }
        ArrayList<Integer> al = new ArrayList<>();

        for (int j = 0; j < nums2.length; j++) {
            if (s.contains(nums2[j])) {
                al.add(nums2[j]);
                s.remove(nums2[j]);
            }
        }

        int arr[] = new int[al.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = al.get(i);
        }
        return arr;
    }

    public static void main(String[] args) {

        int arr1[] = { 4, 9, 5 };
        int arr2[] = { 9, 4, 9, 8, 4 };

        int res[] = Solution.intersection(arr1, arr2);

        for (int x : res) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
