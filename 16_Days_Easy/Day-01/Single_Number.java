/*

Code 1:Single Number

Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
Avizva, epam, cadence, paytm, atlassian,cultfit+7

Platform: LeetCode - 136
Striver’s SDE Sheet

Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.

Example 1:
Input: nums = [2,2,1]
Output: 1

Example 2:
Input: nums = [4,1,2,1,2]
Output: 4

Example 3:
Input: nums = [1]
Output: 1

Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4

Each element in the array appears twice except for one element
which appears only once.

*/

class Solution{

    static int singleNumber(int nums[]){

        
        for(int i=0;i<nums.length;i++){
            
            int count=0;

            for(int j=0;j<nums.length;j++){
                
                if(nums[i]==nums[j]){
                    count++;
                }
            }
            if(count==1){
                return nums[i];
            }
        }
        return -1;
        

    }
    public static void main(String[] args) throws Exception {
        
        int nums[]={2,2,1};

        System.out.println("Result::"+Solution.singleNumber(nums));

    }
}