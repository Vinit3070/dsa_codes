/*
 
Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108

*/

class Solution {

    static int equalSum(int[] A, int N) {

        int lsum = 0;
        int rsum = 0;

        for (int i = 0; i < N; i++) {
            
            rsum=rsum+A[i];
        }
        for(int j=0;j<N;j++){

            rsum=rsum-A[j];
            if(lsum==rsum){
                return j+1;
            }
            lsum=lsum+A[j];
        }

    return -1;
            
    }

    public static void main(String[] args) {

        int arr[] = {1};

        System.out.println(Solution.equalSum(arr, arr.length));

    }
}