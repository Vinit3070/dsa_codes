/*
 Code3 : Equilibrium Point
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n non negative numbers. The task is to find the first
equilibrium point in an array. Equilibrium point in an array is an index (or position) such
that the sum of all elements before that index is the same as the sum of elements after
it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: The equilibrium point is at position 3 as the sum of elements
before it (1+3) = sum of elements after it (2+2).
Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation: Since there's only an element hence its only the equilibrium point.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= A[i] <= 109
*/

class Solution{

    public static int equilibriumPoint(long arr[], int n) {

        long lsum = 0;
        long rsum = 0;

        for (int i = 0; i < n; i++) {
            
            rsum=rsum+arr[i];
        }
        for(int j=0;j<n;j++){

            rsum=rsum-arr[j];
            if(lsum==rsum){
                return j+1;
            }
            lsum=lsum+arr[j];
        }

    return -1;
    }
    public static void main(String[] args) {
        
        long arr[]={1,3,5,2,2};

        System.out.println(Solution.equilibriumPoint(arr,arr.length));

    }
}