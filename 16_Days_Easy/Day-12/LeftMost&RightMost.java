/* 
 Code 3: Left most and right most index
Company: Amazon, Microsoft, Google

Platform: GFG
Description:
Given a sorted array with possibly duplicate elements. The task is to find indexes
of first and last occurrences of an element X in the given array.
Note: If the element is not present in the array return {-1,-1} as pair.
Example 1:
Input:
N = 9
v[] = {1, 3, 5, 5, 5, 5, 67, 123, 125}
X = 5
Output:
2 5
Explanation: Index of first occurrence of 5 is 2 and index of last occurrence of 5
is 5.
Example 2:
Input:
N = 9
v[] = {1, 3, 5, 5, 5, 5, 7, 123, 125}
X = 7
Output:
6 6
Can you solve the problem in expected time complexity?
Expected Time Complexity: O(Log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 105
1 ≤ v[i], X ≤ 1018

*/
class pair  {  
    long first, second;  
    public pair(long first, long second)  
    {  
        this.first = first;  
        this.second = second;  
    }  
}
class Solution{

    static pair indexes(long v[], long x){

        int temp1=-1;
        int temp2=-1;

        for(int i=0;i<v.length;i++){

            if(v[i]==x){
                temp1=i;
                break;
            }
        }

        for(int j=v.length-1;j>=0;j--){

            if(v[j]==x){
                temp2=j;
                break;
            }
        }

        // int i=0;
        // int j=v.length-1;

        // while(i<j){
        //     if(v[i]!=x){
        //         i++;
        //     }
        //     if(v[j]!=x){
        //         j--;
        //     }
        //     if(v[i]==x && v[j]==x){
        //         System.out.println(i+" "+j+" ");
        //         return new pair(i,j);
        //     }
        // }

        // return new pair(-1, -1);

        System.out.println(temp1+" "+temp2+" ");

        return new pair(temp1, temp2);

    }

    public static void main(String[] args) {
        
        long arr[]={1, 3, 5, 5, 5, 5, 67, 123, 125};
        long x=5;

        
        Solution.indexes(arr, x);

    }
}
