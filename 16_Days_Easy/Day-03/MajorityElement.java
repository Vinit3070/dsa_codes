/*

Code 1: Majority Element
Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
Platform : Leetcode - 169, GFG
Fraz’s & striver’s SDE sheet.
Description

Given an array nums of size n, return the majority element.
The majority element is the element that appears more than [n / 2⌋ times. You
may assume that the majority element always exists in the array.
Example 1:
Input: nums = [3,2,3]
Output: 3
Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
Constraints:
n == nums.length
1 <= n <= 5 * 104
-109 <= nums[i] <= 109

*/

class Solution{

    static int majorityElement(int nums[]){

        int maxCnt=0;
        int n=0;

        for(int i=0;i<nums.length;i++){

            int count=0;

            for(int j=0;j<nums.length;j++){

                if(nums[i]==nums[j])
                    count++;
            }

            if(count>maxCnt){

                maxCnt=count;
                n=nums[i];
            }

        }
        if(maxCnt>nums.length/2){
            return n;
        }
        else{
            return -1;
        }
    }
    public static void main(String[] args) {
        
        int arr[]={2,2,1,1,1,2,2};

        System.out.println(Solution.majorityElement(arr));
    }
}