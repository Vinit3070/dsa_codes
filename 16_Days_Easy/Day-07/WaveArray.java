/*

Code2: Wave Array
Company: Paytm, Flipkart, Amazon, Microsoft, FactSet, Goldman Sachs, Google, Adobe
Platform: GFG
Description :
Given a sorted array arr[] of distinct integers. Sort the array into a wave-like array(In
Place).
In other words, arrange the elements into a sequence such that arr[1] >= arr[2] <= arr[3]
>= arr[4] <= arr[5].....
If there are multiple solutions, find the lexicographically smallest one.
Note:The given array is sorted in ascending order, and you don't need to return anything
to make changes in the original array itself.
Example 1:
Input:
n = 5
arr[] = {1,2,3,4,5}
Output: 2 1 4 3 5
Explanation: Array elements after sorting it in wave form are 2 1 4 3 5.
Example 2:
Input:
n = 6
arr[] = {2,4,7,8,9,10}
Output: 4 2 8 7 10 9
Explanation: Array elements after sorting it in wave form are 4 2 8 7 10 9.
Expected Time Complexity: O(n).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ n ≤ 106
0 ≤ arr[i] ≤107

*/

class Solution{

    static void convertToWave(int n, int[] a) {

        if(n%2==0){

            for(int i=0;i<n-1;i=i+2){

                int temp=a[i];
                a[i]=a[i+1];
                a[i+1]=temp;
            }
        }
        else{

            for(int j=0;j<n-1;j=j+2){
                int temp=a[j];
                a[j]=a[j+1];
                a[j+1]=temp;
            }
        }

        for(int x:a){
            System.out.print(x+" ");
        }

    }

    public static void main(String[] args) {

        int arr[]={2,4,7,8,9};

        Solution.convertToWave(arr.length, arr);
    }
}
